#!/bin/sh
sleep 10
# su -m myuser -c "celery -A atena.celery worker -l info"
su -m myuser -c "python manage.py makemigrations"
su -m myuser -c "python manage.py migrate"
su -m myuser -c "python manage.py runserver 0.0.0.0:8000"