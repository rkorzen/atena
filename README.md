# Atena

Atena - to między innymi bogini mądrości. A czemu innemu miałoby służyć zbieranie takich danych jeśli nie tej mądrości:) 

## instalacja i uruchomienie

Należy pobrać repozytorium, utworzyć wirtualne środowisko - najlepiej z pythonem 3.6.
    
    git clone git@bitbucket.org:rkorzen/atena.git
    cd atena
    <teraz należałoby uruchomic/aktywować środowisko wirtualne>
    
    pip install -r requirements-test.txt
    
    (ewentualnie samo requirements.txt, jeśli nie bedziemy odpalać testów)   
   
_(Docelowo chciałbym uruchomić jeszcze kontener dla celery oraz aplikacyjny, 
jednakże pojawiły się tu problemy z komunikacją pomiędzy kontenerami - wiadomości nie trafiały do Brokera. 
Bardzo zagadkowe zachowanie. Niemal identyczna konfiguracja, być może z innymi wersjami zależności działała mi w innym projekcie.
Próba ogarnięcia tego zjadła mi wiele czasu, który mogłem poświęcić na ważniejsze z punktu widzenia zadania rzeczy - stąd też
musiałem w pewnym momencie podjać decyzję o tym, że to zostawiam na później)_ 
    
### Uruchamiamy kontenery z bazą danych i rabbitMQ

Zakładając, że masz docker i docker-compose wykonaj w głównym katalogu projektu

    docker-compose up -d

### Uruchamiamy celery 

    celery -A atena.celery worker -l info

### Uruchamiamy aplikację

    python manage.py runserver

Można też posłużyć się skryptem:

    bash run.sh

Przy pierwszym uruchomieniu potrzebne będzie wykonanie migracji

    python manage.py migrate

Zamiast serwera deweloperskiego django moglibyśmy użyć np. gunicorn

    gunicorn atena.wsgi -b 0.0.0.0:8000

By zadziałało dobrze browsable api z DRF należałoby wtedy jeszcze dodać statyki

    python manage.py collectstatic


## końcówki API

Po uruchomieniu serwera API powinno nas poprowadzić. Główna końcówka wskazuje na dwie gałęzie:

    /picture-tasks
    /text-tasks

Przejście na nie udostępnia nam kolejne możliwości:
W przeglądarkach domyślnie odpalana jest format api, który umożliwia pracę z api z poziomu przeglądarki.
W narzędziach typu Postman domyślnie odpalany jest format json. Można tym sterować poprzez odpowiednie nagłówki,
albo dodanie formatu do url

    ?format=json
    ?format=api

### GET /picture-tasks 

Lista tasków pobrania obrazków. W odpowiedzi widoczna jest lista tasków, każdy z nich ma atrybuty:

    {
        "url": "<url szczegółów zadania - tam będzie lista obrazków>",
        "source_url": <url źródła z któreg pobieramy>,
        "status": "<pending|finished> docelowo też chciałbym dodać tutaj failed",
        "pictures_count": <liczba pobranych obrazków>
    }

### POST /picture-tasks

Tworzenie zadania. Wymaga podania `source_url`

    {
        "source_url": "<url strony>"
    }


### GET /picture-tasks/<id>     

Widok szczegółów zadania. Oprócz wcześniej wymienionych atrybytów widać też:

`"download_zip":"<url>"` - link do pobrania zipa zawierającego wszystkie obrazki dla danego taska

oraz listę obrazków, które mają link do szczegółów, oryginalny url, oraz adres do obrazka w aplikacji:

    "pictures": [
        {
            "url": "<link do szczegółów picture>",
            "original_url": "<oryginalny adres>",
            "image": "<adres w aplikacji>"
        },
        ...
    ]

### GET /text-tasks 

Analogicznie jak w pictures, tyle że dla tasków związanych z tekstem.
Tutaj wszystkie teksty łączone są w jeden. 
Znalezione texty znajdziemy w `text_contents`

    {
        "url": "http://localhost:8000/text-tasks/1/",
        "source_url": "https://www.lipsum.com/feed/html",
        "status": "finished",
        "texts_content": "<znalezione teksty>"
    },

Rozważałem też jakieś dodatkowe możliwości. Np. wskazanie jakiegoś selektora z którego chcielibyśmy pobierać teksty. 
Na pewno sporo ciekawych rzeczy można by tu zrobić, tak by serwis miał większą funkcjonalność

### POST /text-tasks

Tworzenie zadania. Wymaga podania `source_url`

    {
        "source_url": "<url strony>"
    }

### GET /text-tasks/<id>

Szczegóły zadania. Widoczny jest tez link do końcówki umożliwiajacej pobranie zzipowanego tekstu: `download`

### PUT /text-tasks/<id> oraz PUT /picture-tasks/<id>

Zostawiłęm też możlowość zrobienia update. Ta końcówka wymagałaby dopracowania. 
Tak by np. sprawdzać już istniejące obrazki, czy teksty i ewentualnie uaktualniać je o nowe. 
Dodałem w modelu unikalność dla adresu url, wydaje mi się, że nie ma sensu mieć wielu pobrań dla jednego url
Z drugiej jednak strony może akruat byłoby to przydatne do śledzenia ewentualnych zmian. Więc to jest do rozważenia,
ewentualnej zmiany.

### DELETE /text-tasks/<id> oraz DELETE /picture-tasks/<id>

Zostawiłem w API możliwość usuwania poprzez metodę DELETE 

## Modele

W zasadzie nic niezwykłego nie dzieje się w modelach. 
Skorzystałem z sygnałów `post_save`, by odpalać uzupełniać niektóre dane i odpalać taski Celery
Rozważałem dodanie w modelu dodatkowych pól, np. timestampów: created, modified, ale ostatecznie to zostawiłem.
Możliwe, że przydałyby się. 

## Taski

Skorzystałem z BeatufilSoup by wybierać obrazki i teksty. 
W przypadku obrazków uznałem, ze nie ma sensu duplikować ich jeśli występują na stronie wielokrotnie. 

Rozważałem też użycie alternatywnego parsera: https://github.com/rushter/selectolax
Wydawał się być nawet szybszy. Ale z racji tego, ze miałem już zrobioną część związaną z obrazkami przy pomocy BS, 
pozostałem przy tym rozwiązaniu.

## Testy

Na chwilę obecną brakuj mi jeszcze kilku testów. W szczególności tych testujących taski celery. 
Miałem trudności w skonfigurowaniu tego w odpowiedni sposób. Do testów użyłem pytest, który jest mi najlepiej znany.
Pisłem to przy użyciu PyCharma i zauważyłem, że odpalany z terminala zawiesza się na jakimś teście. 
Testy odpalane pojedynczo, lub całymi blokami z poziomu PyCharma zachowywały się prawidłowo
Podejrzewam, że chodzi tu właśnie o taski związane z Celery, być może potrzebna by była tutaj dodatkowa kofiguracja

## Migracje

Zastanawiałem się jeszcze nad poczyszczeniem migracji, połączenia ich w jedną per aplikacja, można by to zrobić przed
przed pierwszym wdrożeniem. 

## Podsumowanie

Dzięki za fajne zadanie. Dało mi sporo frajdy. 
Żałuję, że w ciągu tych 7 dni miałem na pracę przy nim trochę za mało czasu.
Świat stanął na głowie. No ale i tak bywa. 
Trochę żałuję też, że za dużo tego czasu spędziłem nad próbą pełnej dokeryzacji projektu. Powinienem się bardziej skupić 
nad zaplanowaniem i budową samego API. No ale cóż. Trzeba uczyć się na błędach :)
Z punktu widzenia DRF dobrze by było tu jeszcze zastanowić się nad routerami i viewsetami. Przydałaby się zapewne też paginacja
może też jakieś wyszukiwanie/filtrowanie. Zupełnie pominięto tu też problem autentykacji
Być może też bardziej restowo byłoby obsłużyć te pobrania przy pomocy nagłówków, musiałbym trochę o tym doczytać, 
bardziej sie zastanowić, czy skonsultować z kimś mądrzejszym. Zabrakło na to czasu, więc zrobiłem jak umiałem. 
Do zrobienia są tez na pewno takie szczegóły jak rzucanie 404, przeniesienie wrazliwych elementów do zmiennych środowiskowych, czy pliku env itd
No jest jeszcze sporo roboty :)