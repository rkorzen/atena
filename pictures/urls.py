from django.urls import path
from .views import PictureTaskList, PictureTaskDetails, download_pictures, PictureDetails  # PictureList, ,

app_name = "pictures"
urlpatterns = [
    path("", PictureTaskList.as_view(), name="task-list"),
    path("<pk>/", PictureTaskDetails.as_view(), name="task-detail"),
    path("<pk>/download", download_pictures, name="download-all"),
    # path("pictures/", PictureList.as_view(), name="list"),
    path("pictures/<pk>", PictureDetails.as_view(), name="detail"),
]
