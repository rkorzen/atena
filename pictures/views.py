import os
import zipfile
from io import StringIO, BytesIO
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, FileResponse, Http404
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Picture, PictureTask
from .serializers import PictureSerializer, PictureTaskDetailSerializer, PictureTaskListSerializer


class PictureTaskList(generics.ListCreateAPIView):
    queryset = PictureTask.objects.all()
    serializer_class = PictureTaskListSerializer


class PictureTaskDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = PictureTask.objects.all()
    serializer_class = PictureTaskDetailSerializer


# class PictureList(generics.ListAPIView):
#     queryset = Picture.objects.all()
#     serializer_class = PictureSerializer

class PictureDetails(generics.RetrieveAPIView):
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer


def download_pictures(request, pk):
    picture_task = get_object_or_404(PictureTask, pk=pk)

    work_dir = f"media/task_{picture_task.id}"
    files = os.listdir(work_dir)
    files = [os.path.join(work_dir, f) for f in files]
    # zip_file = open(picture_task.download_path, 'rb')
    file_name = f"task_{picture_task.id}_pictures.zip"
    buffer = BytesIO()

    zf = zipfile.ZipFile(buffer, 'w')
    for fpath in files:
        zf.write(fpath, fpath.split("/")[-1])
    zf.close()
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename=file_name)


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'picture-tasks': reverse('pictures:task-list', request=request, format=format),
        'text-tasks': reverse('texts:task-list', request=request, format=format),
    })
