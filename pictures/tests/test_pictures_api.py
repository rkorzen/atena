import pytest
from django.urls import reverse
from rest_framework import status

from pictures.models import PictureTask


@pytest.mark.django_db
class TestPictureTasks:
    def test_main_endpoint(self, client):
        """
        Ensure we can create a new account object.
        """
        url = "http://testserver/"
        response = client.get(url)
        assert response.status_code == 200
        assert response.json() == {"picture-tasks": "http://testserver/picture-tasks/",
                                   "text-tasks": "http://testserver/text-tasks/"}

    def test_get_pictures_tasks_list(self, picture_task, client):
        # client = APIClient()
        url = reverse('pictures:task-list')
        response = client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 1
        assert response.data[0]["source_url"] == 'http://localhost/test.html'
        assert response.data[0]["status"] == PictureTask.PENDING

    @pytest.mark.django_db(transaction=True)
    def test_post_picture_tasks_list(self, client, requests_mock, html_with_images, image_png, celery_app,
                                     celery_worker):
        url = reverse('pictures:task-list')

        response = client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 0

        source_url = "http://localhost/1.png"
        requests_mock.get(url, content=image_png)
        requests_mock.get("http://localhost/test.html", content=html_with_images.encode())

        response = client.post(url, data={"source_url": source_url}, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data["source_url"] == source_url
        assert response.data["status"] == PictureTask.PENDING

        response = client.get(url, format='json')
        assert len(response.data) == 1
        assert response.status_code == status.HTTP_200_OK
        assert response.data[0]["source_url"] == source_url
        assert response.data[0]["status"] == PictureTask.PENDING

    def test_put_picture_tasks_detail(self, picture_task, client):
        url = reverse('pictures:task-detail', args=[picture_task.id])
        data = {"source_url": "http://localhost/2.html"}
        response = client.put(url, data=data, format='json', content_type="application/json")
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 6
        assert response.data["source_url"] == 'http://localhost/2.html'
        assert response.data["status"] == PictureTask.PENDING

    def test_delete_picture_tasks(self, picture_task, client):
        url = reverse('pictures:task-detail', args=[picture_task.id])
        response = client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT
