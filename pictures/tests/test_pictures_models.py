import pytest

from pictures.models import PictureTask, Picture


@pytest.mark.django_db
class TestPictureTask:

    def test_picture_task_create(self):
        pt = PictureTask.objects.create(source_url="http://localhost/test.html")
        assert pt.pk
        assert pt.source_url == "http://localhost/test.html"
        assert pt.status == PictureTask.PENDING
        assert len(pt.pictures.all()) == 0


@pytest.mark.django_db
class TestPicture:

    def test_picture_create(self, picture_task, requests_mock, image_png):
        url = "http://localhost/1.png"
        requests_mock.get(url, content=image_png)
        p = Picture.objects.create(picture_task=picture_task, original_url=url)
        assert p.picture_task == picture_task
        assert p.original_url == url
        assert p.image
