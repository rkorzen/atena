import pytest

from pictures.tasks import get_images_from_page


@pytest.mark.django_db
def test_get_images_from_page(picture_task, image_png, html_with_images, requests_mock):
    url = "http://localhost/1.png"
    requests_mock.get(url, content=image_png)
    requests_mock.get("http://localhost/test.html", content=html_with_images.encode())
    assert picture_task.pictures.count() == 0
    assert picture_task.source_url == "http://localhost/test.html"
    urls = get_images_from_page(picture_task.id)
    assert urls == {"http://localhost/1.png"}
    assert picture_task.pictures.count() == 1
