import requests

from django.db import models
from django.core.files import File
from tempfile import NamedTemporaryFile
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

from pictures.tasks import get_images_from_page
from utils.models import CommonTask


def get_upload_path(instance, filename):
    name, ext = filename.split('.')
    file_path = 'task_{pt_id}/{name}.{ext}'.format(
        pt_id=instance.picture_task.id, name=name, ext=ext)
    return file_path


class PictureTask(CommonTask):

    @property
    def pictures_count(self):
        return self.pictures.count()

    @property
    def download_path(self):
        return f"media/task_{self.id}/"


class Picture(models.Model):
    picture_task = models.ForeignKey(PictureTask, related_name="pictures", on_delete=models.CASCADE)
    original_url = models.URLField()
    image = models.ImageField(upload_to=get_upload_path, null=True, blank=True)

    class Meta:
        unique_together = ("picture_task", "original_url")


@receiver(post_save, sender=PictureTask)
def find_and_make_pictures(sender, instance, **kwargs):
    get_images_from_page.delay(instance.pk)


@receiver(post_save, sender=Picture)
def save_image(sender, instance, **kwargs):
    post_save.disconnect(save_image, sender=sender)
    if instance.original_url and not instance.image:
        image_name = instance.original_url.split("/")[-1]
        if "." in image_name:
            img_temp = NamedTemporaryFile()
            img_temp.write(requests.get(instance.original_url).content)
            img_temp.flush()
            instance.image.save(image_name, File(img_temp))
    instance.save()
    post_save.connect(save_image, sender=sender)
