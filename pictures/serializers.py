from rest_framework import serializers
from .models import Picture, PictureTask


class PictureSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="pictures:detail")

    class Meta:
        model = Picture
        fields = ["url", "original_url", "image"]


class PictureTaskDetailSerializer(serializers.ModelSerializer):
    pictures = PictureSerializer(many=True, read_only=True)
    download_zip = serializers.HyperlinkedIdentityField(view_name="pictures:download-all")

    class Meta:
        model = PictureTask
        fields = ["id", "source_url", "status", "pictures_count", "download_zip", "pictures"]


class PictureTaskListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="pictures:task-detail")

    class Meta:
        model = PictureTask
        fields = ["url", "source_url", "status", "pictures_count"]
