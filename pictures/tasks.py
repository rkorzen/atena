import requests
from bs4 import BeautifulSoup
from celery import shared_task
from django.apps import apps
from urllib.parse import urlparse, urlunparse


@shared_task
def get_images_from_page(picture_task_id):
    picture_task_model = apps.get_model("pictures.PictureTask")
    picture_model = apps.get_model("pictures.Picture")

    picture_task = picture_task_model.objects.get(pk=picture_task_id)
    parsed_url = urlparse(picture_task.source_url)
    main_url = urlunparse([parsed_url.scheme, parsed_url.netloc, "", "", "", ""])

    resp = requests.get(picture_task.source_url)
    soup = BeautifulSoup(resp.text, "html.parser")
    img_tags = soup.find_all("img")
    urls = {img['src'] for img in img_tags}   # only unique images

    for url in urls:
        if "http" not in url:
            url = main_url + url
        if "." in url.split("/")[-1]:
            picture_model.objects.create(picture_task=picture_task, original_url=url)
    picture_task_model.objects.filter(pk=picture_task_id).update(status=picture_task.FINISHED)
    return urls


@shared_task
def debug_task():
    print("Working!!")
