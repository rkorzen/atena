# Generated by Django 3.0.4 on 2020-03-17 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pictures', '0006_auto_20200317_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picturetask',
            name='status',
            field=models.CharField(choices=[('pending', 'pending'), ('finished', 'finished'), ('failed', 'failed')], default='pending', max_length=10),
        ),
    ]
