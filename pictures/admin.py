from django.contrib import admin

# Register your models here.
from .models import PictureTask, Picture


@admin.register(Picture)
class AdminPicture(admin.ModelAdmin):
    pass


@admin.register(PictureTask)
class AdminPictureTask(admin.ModelAdmin):
    list_display = ["source_url", "status"]
