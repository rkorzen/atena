from django.db import models


class CommonTask(models.Model):
    PENDING = "pending"
    FINISHED = "finished"
    FAILED = "failed"
    STATUSES = (
        (PENDING, "pending"),
        (FINISHED, "finished"),
        (FAILED, "failed")
    )
    source_url = models.URLField(unique=True)
    status = models.CharField(max_length=10, choices=STATUSES, default=PENDING)

    class Meta:
        abstract = True
