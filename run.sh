#!/bin/sh
docker-compose up -d
sleep 10
# celery -A atena.celery worker -l info -f
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8000