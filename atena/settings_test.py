from .settings import *

MEDIA_URL = '/media-test/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media-test")

CELERY_TASK_ALWAYS_EAGER = True
