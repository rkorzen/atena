import requests
from bs4 import BeautifulSoup
from celery import shared_task
from django.apps import apps


def get_text(html):
    tree = BeautifulSoup(html, 'lxml')

    body = tree.body
    if body is None:
        return None

    for tag in body.select('script'):
        tag.decompose()
    for tag in body.select('style'):
        tag.decompose()

    text = body.get_text(separator='\n')
    while "\n\n" in text:
        text = text.replace("\n\n", "\n")
    text = text.split("\n")
    text = [t for t in text if t.strip()]

    return "\n".join(text)


@shared_task
def get_texts_from_page(task_id):
    text_task_model = apps.get_model("texts.TextTask")
    text_model = apps.get_model("texts.Text")
    text_task = text_task_model.objects.get(pk=task_id)
    resp = requests.get(text_task.source_url)
    text = get_text(resp.content)
    t = text_model.objects.create(text_task=text_task, content=text)
    text_task_model.objects.filter(pk=task_id).update(status=text_task.FINISHED)
    return t.id
