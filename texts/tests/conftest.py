import pytest
import factory
from pytest_factoryboy import register

from texts.models import TextTask, Text


@pytest.fixture
def html_page():
    html = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
this is test
</body>
</html>"""
    return html


@pytest.fixture
def text_task():
    tt = TextTask.objects.create(source_url="http://localhost/1.html")
    return tt


@pytest.fixture
def text(text_task):
    text = Text.objects.create(text_task=text_task, content="123")
    return text


@pytest.fixture(autouse=True, scope='session')
def celery_config(django_db_setup, django_db_blocker, request):
    with django_db_blocker.unblock():
        return {
            'broker_url': 'amqp://localhost',
            'result_backend': 'default_db',
        }


@register
class PictureTaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TextTask


@pytest.fixture
def text_task():
    pt = PictureTaskFactory(source_url="http://localhost/1.html")
    return pt
