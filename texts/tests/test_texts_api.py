import pytest
from django.urls import reverse
from rest_framework import status

from texts.models import TextTask


@pytest.mark.django_db
class TestTextTasks:
    def test_main_endpoint(self, client):
        """
        Ensure we can create a new account object.
        """
        url = "http://testserver/"
        response = client.get(url)
        assert response.status_code == 200
        assert response.json() == {"picture-tasks": "http://testserver/picture-tasks/",
                                   "text-tasks": "http://testserver/text-tasks/"}

    def test_get_text_tasks_list(self, text_task, client):
        # client = APIClient()
        url = reverse('texts:task-list')
        response = client.get(url, format='json')
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 1
        assert response.data[0]["source_url"] == 'http://localhost/1.html'
        assert response.data[0]["status"] == TextTask.PENDING

    # TODO: ten test zawisa, trzeba to naprawić
    # @pytest.mark.django_db(transaction=True)
    # def test_post_text_tasks_list(self, client, requests_mock, html_page, celery_app, celery_worker):
    #     url = reverse('texts:task-list')
    #
    #     response = client.get(url, format='json')
    #     assert response.status_code == status.HTTP_200_OK
    #     assert len(response.data) == 0
    #
    #     source_url = "http://localhost/1.html"
    #     requests_mock.get(source_url, content=html_page.encode())
    #     response = client.post(url, data={"source_url": source_url}, format='json')
    #     assert response.status_code == status.HTTP_201_CREATED
    #     assert response.data["source_url"] == source_url
    #     assert response.data["status"] == TextTask.PENDING
    #
    #     response = client.get(url, format='json')
    #     assert len(response.data) == 1
    #     assert response.status_code == status.HTTP_200_OK
    #     assert response.data[0]["source_url"] == source_url
    #     assert response.data[0]["status"] == TextTask.PENDING

    def test_put_text_tasks_detail(self, text_task, client):
        url = reverse('texts:task-detail', args=[text_task.id])
        data = {"source_url": "http://localhost/2.html"}
        response = client.put(url, data=data, format='json', content_type="application/json")
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 5
        assert response.data["source_url"] == 'http://localhost/2.html'
        assert response.data["status"] == TextTask.PENDING

    def test_delete_text_tasks(self, text_task, client):
        url = reverse('texts:task-detail', args=[text_task.id])
        response = client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT
