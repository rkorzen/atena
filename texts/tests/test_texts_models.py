import pytest


# Create your tests here.
from texts.models import TextTask, Text


@pytest.mark.django_db
def test_text_task_create():
    tt = TextTask.objects.create(source_url="http://localhost/1.html")
    assert tt.pk
    assert tt.source_url == "http://localhost/1.html"
    assert tt.status == TextTask.PENDING
    assert len(tt.texts.all()) == 0


@pytest.mark.django_db
def test_create_text(text_task):
    t = Text.objects.create(text_task=text_task, content="to jest test")
    assert t.text_task == text_task
    assert t.content == "to jest test"
