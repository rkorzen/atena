import pytest
from texts.models import TextTask, Text
from texts.tasks import get_text, get_texts_from_page


def test_get_text(html_page):
    expected = "this is test"
    assert get_text(html_page) == expected

# # TODO: ten test zawisa, trzeba to naprawić
# @pytest.mark.django_db(transaction=True)
# def test_get_texts_from_page(text_task, celery_app, celery_worker):
#     assert text_task.status == TextTask.PENDING
#     t_id = get_texts_from_page.delay(text_task.id).get(timeout=10)
#     text = Text.objects.get(pk=t_id)
#     assert text.text_task.status == TextTask.FINISHED
