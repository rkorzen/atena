from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

from texts.tasks import get_texts_from_page
from utils.models import CommonTask


class TextTask(CommonTask):

    @property
    def texts_content(self):
        texts = self.texts.values("content")
        texts_container = []
        for t in texts:
            texts_container += t.values()

        return "\n".join(texts_container)


class Text(models.Model):
    text_task = models.ForeignKey(TextTask, on_delete=models.CASCADE, related_name="texts")
    content = models.TextField()


@receiver(post_save, sender=TextTask)
def save_texts(sender, instance, **kwargs):
    get_texts_from_page.delay(instance.pk)
