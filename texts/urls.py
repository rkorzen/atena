from django.urls import path
from texts.views import TextTaskList, TextTaskDetails, download_texts

app_name = "texts"
urlpatterns = [
    path("", TextTaskList.as_view(), name="task-list"),
    path("<pk>/", TextTaskDetails.as_view(), name="task-detail"),
    path("<pk>/download", download_texts, name="download"),

]
