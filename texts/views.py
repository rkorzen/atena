import zipfile

from django.http import HttpResponse
from rest_framework import generics

from .models import TextTask
from .serializers import TextTaskDetailSerializer, TextTaskListSerializer


class TextTaskList(generics.ListCreateAPIView):
    queryset = TextTask.objects.all()
    serializer_class = TextTaskListSerializer


class TextTaskDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = TextTask.objects.all()
    serializer_class = TextTaskDetailSerializer


def download_texts(request, pk):
    test_task = TextTask.objects.get(pk=pk)
    file_name = f"task_{test_task.id}_texts.txt"
    zip_file_name = f"task_{test_task.id}_texts.zip"

    response = HttpResponse(content_type='application/zip')
    zf = zipfile.ZipFile(response, 'w')
    zf.writestr(file_name, test_task.texts_content)

    # return as zipfile
    response['Content-Disposition'] = f'attachment; filename={zip_file_name}'
    return response
