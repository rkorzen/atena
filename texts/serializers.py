from rest_framework import serializers
from .models import Text, TextTask


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ["id", "content"]


class TextTaskDetailSerializer(serializers.ModelSerializer):
    # texts = TextSerializer(many=True, read_only=True)
    download = serializers.HyperlinkedIdentityField(view_name="texts:download")

    class Meta:
        model = TextTask
        fields = ["id", "source_url", "status", "download", "texts_content"]


class TextTaskListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="texts:task-detail")

    class Meta:
        model = TextTask
        fields = ["url", "source_url", "status", "texts_content"]
