# Generated by Django 3.0.4 on 2020-03-17 19:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TextTask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('source_url', models.URLField(unique=True)),
                ('status', models.CharField(choices=[('pending', 'pending'), ('finished', 'finished'), ('failed', 'failed')], default='pending', max_length=10)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.TextField()),
                ('text_task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='texts', to='texts.TextTask')),
            ],
        ),
    ]
